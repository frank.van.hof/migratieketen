# Stage 1
FROM node:20-alpine3.19 AS node_builder

RUN corepack enable && corepack prepare pnpm@latest --activate

# Copy the code into the container. Note: copy to a dir instead of `.`, since Parcel cannot run in the root dir, see https://github.com/parcel-bundler/parcel/issues/6578
WORKDIR /build
COPY package.json pnpm-lock.yaml tailwind.config.js .postcssrc ./

RUN pnpm install

COPY static static
COPY public public

# Build the static files
RUN pnpm run build


# Stage 2: serve the files using nginx
FROM nginx:1.25.3-alpine3.18

COPY --from=node_builder /build/public /usr/share/nginx/html

# COPY ./nginx.conf /etc/nginx/conf.d/default.conf

# Copy the correct version of the index file to index.html
ARG VERSION="remote"

COPY public/index-${VERSION}.html /usr/share/nginx/html/index.html

EXPOSE 80
