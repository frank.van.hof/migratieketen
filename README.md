# Migratieketen Fieldlab

Monorepo containing all Migratieketen Fieldlab 15 t/m 17 januari 2024 code.

## Overview

TODO: Project overview.

## Developer documentation

If you would like to contribute to this project, consult the [`CONTRIBUTING.md`](CONTRIBUTING.md) file.


## License

Copyright © VNG Realisatie 2023

[Licensed under the EUPLv1.2](LICENSE)

[You can find more information about the license here](https://commission.europa.eu/content/european-union-public-licence_en).
