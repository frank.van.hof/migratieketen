package config

import (
	"fmt"
	"os"

	"github.com/iamolegga/enviper"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v3"
)

type Attribute struct {
	Name     string      `yaml:"name"`
	Label    string      `yaml:"label"`
	Type     string      `yaml:"type"`
	Multiple bool        `yaml:"multiple"` // For types 'select' and 'file'
	Default  interface{} `yaml:"default"`

	// For number types
	Min  *interface{} `yaml:"min"`
	Max  *interface{} `yaml:"max"`
	Step *interface{} `yaml:"step"`

	// For select, radios, and checkboxes types
	Options []struct {
		Name  string `yaml:"name"`
		Label string `yaml:"label"`
	} `yaml:"options"`
}

type Rubriek struct {
	Label string `yaml:"label"`

	Attributes []Attribute `yaml:"attributes"`
}

type GlobalConfig struct {
	Rubrieken []Rubriek `yaml:"rubrieken"`
}

type Config struct {
	GlobalConfig         `mapstructure:",squash"`
	BackendListenAddress string
	BvvApiServerUrl      string
	SigmaSourceUrls      map[string]string
}

// New composes a config with values from the specified paths
func New(globalConfigPath string, configPath string) (*Config, error) {
	e := enviper.New(viper.New())
	e.AllowEmptyEnv(true)
	e.AutomaticEnv()
	e.SetEnvPrefix("APP")
	e.SetDefault("BACKENDLISTENADDRESS", ":8080")
	// RH: TODO: don't hardcode defaults, this should come from the config file.
	e.SetDefault("SIGMASOURCEURLS", map[string]string{
		"coa": "http://np-sigma-backend-svc/v0",
		"ind": "http://np-sigma-backend-svc/v0",
	})

	config := new(Config)
	if err := e.Unmarshal(config); err != nil {
		panic("unable to decode into config struct")
	}
	if !e.IsSet("BVVAPISERVERURL") {
		panic("missing mandatory APP_BVVAPISERVERURL config")
	}
	if !e.IsSet("SIGMASOURCEURLS") {
		panic("missing mandatory APP_SIGMASOURCEURLS config")
	}

	if err := readConfig(globalConfigPath, &config.GlobalConfig); err != nil {
		return nil, err
	}

	return config, nil
}

func readConfig(path string, cfg any) error {
	file, err := os.ReadFile(path)
	if err != nil {
		return fmt.Errorf("error reading config file: %w", err)
	}

	if err := yaml.Unmarshal(file, cfg); err != nil {
		return fmt.Errorf("unmarshalling config failed: %w", err)
	}

	return nil
}
