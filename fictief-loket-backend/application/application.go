package application

import (
	"context"
	"fmt"
	"log/slog"
	"net/http"
	"slices"
	"sync"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"golang.org/x/exp/maps"
	"golang.org/x/sync/errgroup"

	api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-loket-backend/config"
	sigma_api "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
)

type Application struct {
	*http.Server
	logger *slog.Logger
	cfg    *config.Config
}

// Slice type that can be safely shared between goroutines
type ConcurrentSlice[T any] struct {
	sync.RWMutex
	items []T
}

func (s *ConcurrentSlice[T]) Append(v T) {
	s.Lock()
	s.items = append(s.items, v)
	s.Unlock()
}

func (s *ConcurrentSlice[T]) Length() int {
	s.RLock()
	l := len(s.items)
	s.RUnlock()
	return l
}

func (s *ConcurrentSlice[T]) Iter() <-chan T { // Not super happy with the current implementation since the iteration could block the whole slice
	c := make(chan T)

	go func() {
		s.RLock()
		defer s.RUnlock()

		for _, v := range s.items {
			c <- v
		}

		close(c)
	}()

	return c
}

// ReadProcessenByVreemdelingId implements api.StrictServerInterface.
func (app *Application) ReadProcessenByVreemdelingId(ctx context.Context, request api.ReadProcessenByVreemdelingIdRequestObject) (api.ReadProcessenByVreemdelingIdResponseObject, error) {
	sources := maps.Keys(app.cfg.SigmaSourceUrls)
	if request.Params.Sources != nil && !slices.Equal(*request.Params.Sources, api.QuerySources{""}) {
		sources = *request.Params.Sources
	}

	processes := ConcurrentSlice[api.Proces]{}

	g, gctx := errgroup.WithContext(ctx)
	for _, source := range sources {

		source := source

		g.Go(func() error {
			client, err := sigma_api.NewClientWithResponses(app.cfg.SigmaSourceUrls[source])
			if err != nil {
				return err
			}

			resp, err := client.ReadProcessenByVreemdelingIdWithResponse(gctx, request.VreemdelingId)
			if err != nil {
				return err
			}

			if resp.JSON200 == nil {
				return fmt.Errorf("failed to call sigma source '%s': %d", source, resp.StatusCode())
			}

			for _, process := range resp.JSON200.Data {
				attributes := api.ProcesAttribute{}
				for k, attr := range process.Attributes {
					procAttr := attributes[k]

					procAttr.Value = attr.Value

					for _, a := range attr.Attributes {
						procAttr.Attributes = append(procAttr.Attributes, (api.Attribute)(a))
					}

					attributes[k] = procAttr
				}

				processes.Append(api.Proces{
					Attributes: attributes,
					CreatedAt:  process.CreatedAt,
					DeletedAt:  process.DeletedAt,
					Id:         process.Id,
					ProcesId:   process.ProcesId,
					Status:     process.Status,
					Type:       process.Type,
				})
			}

			return nil
		})
	}

	if err := g.Wait(); err != nil {
		return nil, err
	}

	proc := make([]api.Proces, 0, processes.Length())
	for a := range processes.Iter() {
		proc = append(proc, a)
	}

	return api.ReadProcessenByVreemdelingId200JSONResponse{
		ProcessenResponseJSONResponse: api.ProcessenResponseJSONResponse{
			Data: proc,
		},
	}, nil
}

func New(logger *slog.Logger, cfg *config.Config) Application {
	app := Application{
		Server: &http.Server{
			Addr: cfg.BackendListenAddress,
		},
		logger: logger,
		cfg:    cfg,
	}
	app.initSupportedAttributes()
	return app
}

func (app *Application) Router() {
	r := chi.NewRouter()

	r.Use(middleware.Recoverer)
	r.Use(middleware.Heartbeat("/healthz"))
	r.Use(middleware.Logger)
	r.Use(middleware.SetHeader("Content-Type", "application/json"))

	apiHandler := api.NewStrictHandler(app, nil)
	r.Mount("/v0", api.Handler(apiHandler))

	r.Group(func(r chi.Router) {
		r.Use(middleware.SetHeader("Content-Type", "application/yaml"))
		r.Get("/openapi.yaml", app.OpenAPI)
	})

	app.Server.Handler = r
}
