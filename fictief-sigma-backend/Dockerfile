FROM golang:1.21.5-alpine3.19 as builder

# Cache dependencies
RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

WORKDIR /build/fictief-sigma-api
COPY fictief-sigma-api/go.mod fictief-sigma-api/go.sum ./

WORKDIR /build/fictief-
COPY fictief-sigma-api/go.mod fictief-sigma-api/go.sum ./

WORKDIR /build/fictief-sigma-backend
COPY fictief-sigma-backend/go.mod fictief-sigma-backend/go.sum ./

RUN go mod download

## Build the Go Files
WORKDIR /build/fictief-sigma-api
COPY fictief-sigma-api ./

WORKDIR /build/config
COPY config ./

WORKDIR /build/fictief-sigma-backend
COPY fictief-sigma-backend ./

RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-s -w" -o /build/server

## Run the server for dev
ENTRYPOINT CompileDaemon -log-prefix=false -build="go build -o server ." -command="./server serve"


FROM alpine:3.19

# Add timezones
RUN apk add --no-cache tzdata

# Copy the bin from builder to root.
COPY --from=builder /build/server /app/server
COPY config /config

WORKDIR /app

ENTRYPOINT ["/app/server", "serve"]

EXPOSE 8080
