FROM golang:1.21.5-alpine3.19

RUN ["go", "install", "github.com/githubnemo/CompileDaemon@latest"]

# Install Node.js. Note: the nodejs package does not include corepack, so we use nodejs-current
RUN apk add --no-cache nodejs-current

RUN corepack enable && corepack prepare pnpm@latest --activate

WORKDIR /build/frontend

# Cache dependencies
COPY frontend/go.mod frontend/go.sum ./

RUN go mod download

COPY frontend/package.json frontend/pnpm-lock.yaml frontend/tailwind.config.js frontend/.postcssrc frontend/.parcelrc ./

RUN pnpm install

COPY frontend/main.go .
COPY frontend/application application
COPY frontend/cmd cmd
COPY frontend/config config
COPY frontend/helpers helpers

COPY frontend/views views
COPY frontend/static static

COPY config /build/config

ENTRYPOINT sh -c 'pnpm run dev & CompileDaemon -log-prefix=false -pattern="(.+\.go|.+\.html|.+\.js|.+\.css|.+\.sql)$" -exclude-dir=.git -exclude-dir=node_modules -exclude-dir=.parcel-cache -build="go build -o server ." -command="./server serve"'
