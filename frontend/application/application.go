package application

import "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/config"

type Application struct {
	Cfg *config.Config
}

func New(cfg *config.Config) Application {
	return Application{
		Cfg: cfg,
	}
}
