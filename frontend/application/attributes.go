package application

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	sigma "gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/fictief-sigma-api"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/config"
	"gitlab.com/digilab.overheid.nl/ecosystem/migratieketen/frontend/helpers"
)

func (a *Application) parseAttribute(values []string, attr *config.Attribute) (val sigma.ObservationCreate_Value, err error) {
	switch attr.Type {
	// String values
	case "text", "textarea", "radios":
		{
			if len(values) == 1 { // Regard as unset/null in case of no or multiple values
				if err = val.FromObservationCreateValue1(values[0]); err != nil {
					err = fmt.Errorf("error setting observation value: %w", err)
					return
				}
			}
		}

	// Boolean values
	case "boolean":
		if len(values) != 1 {
			err = errors.New("unexpected number of values")
			return
		}

		if err = val.FromObservationCreateValue2(values[0] == "true"); err != nil {
			err = fmt.Errorf("error setting observation value: %w", err)
			return
		}

	// Number values
	case "number":
		if len(values) != 1 {
			err = errors.New("unexpected number of values")
			return
		}

		if values[0] != "" { // Regard as unset/null when empty
			var num float64
			if num, err = strconv.ParseFloat(values[0], 32); err != nil {
				err = fmt.Errorf("error parsing number: %w", err)
				return
			}

			if err = val.FromObservationCreateValue4(float32(num)); err != nil {
				err = fmt.Errorf("error setting observation value: %w", err)
				return
			}
		}

	// List values
	case "checkboxes":
		// Convert the []string slice to []interface{}
		vals := make([]any, len(values))
		for i, v := range values {
			vals[i] = v
		}

		if err = val.FromObservationCreateValue3(vals); err != nil {
			err = fmt.Errorf("error setting observation value: %w", err)
			return
		}

	// Select boxes: differentiate between Multiple true/false
	case "select":
		if attr.Multiple {
			// Convert the []string slice to []interface{}
			vals := make([]any, len(values))
			for i, v := range values {
				vals[i] = v
			}

			if err = val.FromObservationCreateValue3(vals); err != nil {
				err = fmt.Errorf("error setting observation value: %w", err)
				return
			}
		} else {
			if len(values) != 1 {
				err = errors.New("unexpected number of values")
				return
			}

			if err = val.FromObservationCreateValue1(values[0]); err != nil {
				err = fmt.Errorf("error setting observation value: %w", err)
				return
			}
		}

	// Date(time) values
	case "date":
		if len(values) != 1 {
			err = errors.New("unexpected number of values")
			return
		}

		// Parse as FormattedDate
		var d helpers.FormattedDate
		if err = d.UnmarshalText([]byte(values[0])); err != nil {
			err = fmt.Errorf("error parsing date: %w", err)
			return
		}

		// Set the re-formatted date as value
		if err = val.FromObservationCreateValue1(d.Format(time.DateOnly)); err != nil {
			err = fmt.Errorf("error setting observation value: %w", err)
			return
		}

	case "datetime":
		if len(values) != 1 {
			err = errors.New("unexpected number of values")
			return
		}

		// Parse as FormattedTime
		var dt helpers.FormattedTime
		if err = dt.UnmarshalText([]byte(values[0])); err != nil {
			err = fmt.Errorf("error parsing date: %w", err)
			return
		}

		// Set the re-formatted time as value
		if err = val.FromObservationCreateValue1(time.Time(dt).Format(time.RFC3339)); err != nil {
			err = fmt.Errorf("error setting observation value: %w", err)
			return
		}

	// Fallback: encode as JSON
	default:
		if len(values) != 1 {
			err = errors.New("unexpected number of values")
			return
		}

		if err = val.UnmarshalJSON([]byte(values[0])); err != nil {
			err = fmt.Errorf("error unmarshalling observation value: %w", err)
			return
		}
	}

	return
}
